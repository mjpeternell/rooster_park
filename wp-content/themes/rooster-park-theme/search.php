<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package RoosterPark
 * @since RoosterPark 1.0
 */
get_header();
?>
<div id="primary" class="content-area">
    <main id="content" class="site-content" role="main">
        <div class="col-fullbleed white">
            <div class="col-full rooster-search">
                <div class="column-6 offset-1">
                    <?php if (have_posts()) : ?>

                        <header class="page-header">
                            <h1 class="page-title"><?php printf(__('Search Results for: %s', 'rooster_park'), '<span>' . get_search_query() . '</span>'); ?></h1>
                        </header><!-- .page-header -->

                        <?php //rooster_park_content_nav('nav-above'); ?>

                        <?php /* Start the Loop */ ?>
                        <?php while (have_posts()) : the_post(); ?>

                            <?php get_template_part('content', 'search'); ?>

                        <?php endwhile; ?>

                        <?php rooster_park_content_nav('nav-below'); ?>

                    <?php else : ?>

                        <?php get_template_part('no-results', 'search'); ?>

                    <?php endif; ?>
                </div>
                <div class="column-4">
                    <?php get_sidebar(); ?>  
                </div>

            </div>
        </div>
    </main>
</div>
<?php get_footer(); ?>