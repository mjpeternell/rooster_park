/*! Rooster Park Theme  - v0.1.0 - 2017-12-05
 * http://roosterparkDotcom
 * Copyright (c) 2017; * Licensed GPLv2+ */



/*! RoosterPark - roosterparks.js - v0.1.0 - 2016-04-01
 * http://roosterparkDotcom
 * Copyright (c) 2016; * Licensed GPLv2+ */

jQuery(document).ready(function () {

    /** In View https://github.com/protonet/jquery.inview **/
    jQuery('#myCounter').on('inview', function (event, isInView) {
        if (isInView) {
            //console.log("Counter in view");
            /* === COUNT FACTORS - https://github.com/mhuggins/jquery-countTo === */
            var dataperc;
            jQuery('.fact').each(function () {
                dataperc = jQuery(this).attr('data-perc'),
                        jQuery(this).find('.factor').delay(10000).countTo({
                    from: 0,
                    to: dataperc,
                    speed: 3000,
                    refreshInterval: 50
                });
            });
        } else {
            //console.log("Counter Not In View");
        }
    });

    //truncate stings
    //var teamBio = jQuery('.meet-our-team .tile-cell .popup-content > p');

    jQuery('.meet-our-team .tile-cell .popup-content > p').succinct({
        size: 385
    });

    jQuery('#Rooster-Blog .blog-content p').succinct({
        size: 190
    });

    jQuery('#Rooster-Blog h1 a.truncate').succinct({
        size: 40
    });
    
//    jQuery('.cs-logo-list-item p.truncate').succinct({
//        size: 150
//    });

    jQuery('#Case-Studies-Home .h2-truncate').succinct({
        size: 50
    });

    jQuery('#Case-Studies-Home p').succinct({
        size: 350
    });

    jQuery('#testimonials p').succinct({
        size: 350
    });
    jQuery('#We-Do-Both p').succinct({
        size: 250
    });

    //scroll to top functionality
    jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() > 100) {
            jQuery('.scroll-to-top').fadeIn();
        } else {
            jQuery('.scroll-to-top').fadeOut();
        }
    });

    //Click event to scroll to top
    jQuery('.scroll-to-top').click(function () {
        jQuery('html, body').animate({scrollTop: 0}, 800);
        return false;
    });

    window.onorientationchange = function () {
        window.location.reload();
    };

//triggered when modal is about to be shown
    jQuery('.mod-pop button').on('click', function (e) {
        //get data-id attribute of the clicked element

        //populate the textbox

    });

    jQuery('a[name=\'modal\']').click(function (e) {

        //Cancel the link behavior
        e.preventDefault();
        //Get the A tag
        var id = jQuery(this).attr('href');

        //popup opening and closing fade speed
        var popup_open_speed = 300;

        //Get the window height and width
        var winH = jQuery(window).height();
        var winW = jQuery(window).width();
        //console.log(winH + ' - ' + winW);
        //
        //Set the popup window to center
        if (winW <= 680) {
            jQuery(id).css('top', 40);
        } else {
            jQuery(id).css('top', winH / 2 - jQuery(id).height() / 2);
        }
        jQuery(id).css('left', winW / 2 - jQuery(id).width() / 2);

        var theTitle = jQuery(this).attr('title');
        var theJobTitle = "";
        var popJobTitle = jQuery(".popup-job-title").length;

        if (popJobTitle !== 0) {
            theJobTitle = jQuery(this).siblings(".popup-job-title").text();
        }

        var theContent = jQuery(this).siblings(".popup-content").html();

        jQuery(id).find('header h2').append(theTitle);
        jQuery(id).find('header h3').append(theJobTitle).show();
        jQuery(id).find('.content').append(theContent);

        //Set the popup window to center
        if (winW <= 680) {
            jQuery(id).css('top', 40);
        } else {
            jQuery(id).css('top', winH / 2 - jQuery(id).height() / 2);
        }
        jQuery(id).css('left', winW / 2 - jQuery(id).width() / 2);

        //transition effect
        jQuery('#modal_screen').fadeIn(popup_open_speed);
        jQuery(id).fadeIn(popup_open_speed);

    });

//if close button is clicked
    jQuery('.modalwindow .close-btn').click(function (e) {

        var mod_id = jQuery('.modalwindow');
        
        //popup opening and closing fade speed
        var popup_close_speed = 300;
        
        jQuery('.modalwindow').find('header h2').empty();
        jQuery('.modalwindow').find('header h3').empty().hide();
        jQuery('.modalwindow').find('.content p').remove();

        //Cancel the link behavior
        e.preventDefault();

        jQuery('.modalwindow').fadeOut(popup_close_speed);
        jQuery('#modal_screen').fadeOut(popup_close_speed);
    });

    jQuery(window).resize(function () {
        var more = document.getElementById("js-navigation-more");
        if (jQuery(more).length > 0) {
            var windowWidth = $(window).width();
            var moreLeftSideToPageLeftSide = jQuery(more).offset().left;
            var moreLeftSideToPageRightSide = windowWidth - moreLeftSideToPageLeftSide;

            if (moreLeftSideToPageRightSide < 330) {
                jQuery("#js-navigation-more .submenu .submenu").removeClass("fly-out-right");
                jQuery("#js-navigation-more .submenu .submenu").addClass("fly-out-left");
            }

            if (moreLeftSideToPageRightSide > 330) {
                jQuery("#js-navigation-more .submenu .submenu").removeClass("fly-out-left");
                jQuery("#js-navigation-more .submenu .submenu").addClass("fly-out-right");
            }
        }
    });


    var menuToggle = jQuery("#js-mobile-menu").unbind();


    menuToggle.on("click", function (e) {
        e.preventDefault();
        jQuery("#js-navigation-menu").slideToggle(function () {
            jQuery("#js-navigation-menu").toggleClass("show");
            jQuery("#js-mobile-menu").toggleClass("active");
        });
    });

    //Smooth Scorll Functionality
    // Scroll To Functionality
    jQuery(function () {
        // your current click function
        jQuery('#hero-btn, .scroll').on('click', function (e) {
            e.preventDefault();
            jQuery('html, body').animate({
                scrollTop: jQuery(jQuery(this).attr('href')).offset().top + 'px'
            }, 1000, 'swing');
        });

        jQuery('.btn.single-nav-link[href*="#"]:not([href="#"])').click(function () {
            if (window.location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && window.location.hostname === this.hostname) {
                var target = jQuery(this.hash);
                target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    jQuery('html,body').animate({
                        scrollTop: (target.offset().top)
                    }, 1000);
                    return false;
                }
            }
        });

        // *only* if we have anchor on the url
        if (window.location.hash) {
            // smooth scroll to the anchor id
            jQuery('html, body').animate({
                scrollTop: jQuery(window.location.hash).offset().top + 'px'
            }, 1000, 'swing');
        }

    });

});
