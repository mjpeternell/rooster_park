
<?php
$gmap_cta_hero_image = get_field('gmap_cta_hero_image');
$gmap_cta_hero_image_mobile = get_field('gmap_cta_hero_image_mobile');

if (!empty($gmap_cta_hero_image)):
    $size = 'hero-cta-1400';
    $url = $gmap_cta_hero_image['url'];
    $title = $gmap_cta_hero_image['title'];
    $alt = $gmap_cta_hero_image['alt'];
endif;
if (!empty($gmap_cta_hero_image_mobile)):
    $size = 'hero-cta-1400';
    $url_mob = $gmap_cta_hero_image_mobile['url'];
    $title_mob = $gmap_cta_hero_image_mobile['title'];
    $alt_mob = $gmap_cta_hero_image_mobile['alt'];
endif;
?>
<div class="hero static">
    <?php if (!empty($gmap_cta_hero_image)): ?>
        <img class="img-responsive desktop-hero" src="<?php echo $url; ?>" alt="<?php echo $alt; ?>">
    <?php endif; ?>
    <?php if (!empty($gmap_cta_hero_image_mobile)): ?>
        <img class="img-responsive mobile-hero" src="<?php echo $url_mob; ?>" alt="<?php echo $alt_mob; ?>">
    <?php endif; ?>
</div>
<section class="col-fullbleed dust gmap-cta">
    <div class="col-full capability">
        <?php
        $location = get_field('gmaps_location');
        $more_contact_info = get_field('more_contact_info');

        $myAddress = $location['address'];
        ?>
        <?php if (get_field('google_map_cta_title')): ?>
            <h1><?php the_field('google_map_cta_title'); ?></h1>
        <?php endif; ?>
        <?php if (!empty($more_contact_info)): ?>
            <div class="entry-content"><?php echo $more_contact_info; ?></div>
        <?php endif; ?>

            <div class="acf-map-new">
                <a href="https://www.google.com/maps/place/Rooster+Park/@47.6378849,-122.3404088,17.16z/data=!4m5!3m4!1s0x0:0xdff40acb9ac9bbc0!8m2!3d47.6382809!4d-122.3402954" target="_blank">
                    <img src="https://www.roosterpark.com/wp-content/uploads/2019/11/rp-hq-map-2.jpg" class="map-image"/>
                </a>

            </div>
            <p style="margin-top: 1em; text-align: center;"><a href="" target="_blank"><strong>Google Maps Link</strong></a></p>

    </div>
</section>
