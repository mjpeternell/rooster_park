    <?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<section id="capabilitiesDevServ" class = "col-fullbleed capDevServ dust">
    <div class="col-full capability">
        <h1 class="section-header align-center">Development Services</h1>

        <?php
        // Start the page loop.
        while (have_posts()) : the_post();
        // Include the page content template.
        //get_template_part('content', 'page');
        // End the loop.
        endwhile;
        ?>
        <div class="row flexy">
            <!-- Button trigger modal -->

            <?php
            $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
            $blog_arg = array(
                'post_type' => 'engineering',
                'orderby' => 'date',
                'order' => 'ASC',
                'post_status' => 'publish',
                'posts_per_page' => 12,
                'paged' => $paged,
            );
            $wp_blog_query = new WP_Query($blog_arg);
            $postx_counter = -1;
            if (have_posts()) :
                while ($wp_blog_query->have_posts()) : $wp_blog_query->the_post();
                    $postx_counter++;
                    $offset = "";
                    if ($postx_counter == 0) {
                        $offset = "col-lg-offset-2";
                    } else {
                        $offset = "col-lg-offset-1";
                    }
                    ?>
                    <div  class="tile-cell bg" data-count="<?php echo $postx_counter; ?>">
                        <article id="modal-content-<?php echo $postx_counter; ?>" class="mod-pop" >
                            <?php if (has_post_thumbnail()) { ?>
                            <a href="#modal" name="modal"  title="<?php the_title(); ?>"><?php the_post_thumbnail('full', array('class' => "img-responsive")); ?></a>
                            <?php } else { ?>
                            <a href="#modal" name="modal"  title="<?php the_title(); ?>"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/images/blog-placeholder-image-480x350.jpg'; ?>" class="img-responsive"/></a>
                            <?php } ?>
<!--                            <a class="truncate" href="<?php //the_permalink(); ?>" name="link"><?php //the_title(); ?></a>-->
<!--                            <div class="title-dev truncate"><?php the_title(); ?></div>-->
                            <a class="truncate" href="#modal" name="modal"  title="<?php the_title(); ?>"><?php the_title(); ?></a>
                            <div class="popup-title">
                                <?php the_title(); ?>
                            </div>    
                            <div class="popup-content">
                                <?php the_content(); ?>
                            </div>
                            <?php edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>'), '<p class="edit">', '</p>', 0, 'post-edit-link btn btn-default'); ?>
                        </article>
                    </div>
                <?php endwhile; ?>
            </div>  
            <?php if ($wp_blog_query->max_num_pages > 1) { // check if the max number of pages is greater than 1   ?>
                <section>
                    <div class="row">
                        <nav class="post-navigation col-lg-12">
                            <div class="nav-previous">
                                <?php echo get_next_posts_link('<i class="fa fa-angle-left" aria-hidden="true"></i> Older Entries', $wp_blog_query->max_num_pages); // display older posts link   ?>
                            </div>
                            <div class="nav-next">
                                <?php echo get_previous_posts_link('Newer Entries <i class="fa fa-angle-right" aria-hidden="true"></i>'); // display newer posts link   ?>
                            </div>
                        </nav>
                    </div>
                </section>
                <?php
            }
        endif;
        ?>
        <?php wp_reset_postdata(); ?>


    </div>


</section>