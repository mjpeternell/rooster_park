<?php
/*
 * Footer CTA: includes ACF Hero image, title and link.
 * Pages: Homepage,
 * 
 */
?>
<!-- Secondary Hero/Footer CTA -->
<div class="footer-cta">
    <?php
    $cta_image = get_field('cta_slide', 'option');
    $cta_image_mobile = get_field('cta_slide_mobile', 'option');

    if (!empty($cta_image)):
        $cta_url = $cta_image['url'];
        $cta_alt = $cta_image['alt'];
        ?>
        <img class="img-responsive desktop-hero" src="<?php echo $cta_url; ?>" alt="<?php echo $cta_alt; ?>">
    <?php endif; ?>
    <?php
    if (!empty($cta_image_mobile)):
        $cta_mob_url = $cta_image_mobile['url'];
        $cta_mob_alt = $cta_image_mobile['alt'];
        ?>
        <img class="img-responsive mobile-hero" src="<?php echo $cta_mob_url; ?>" alt="<?php echo $cta_mob_alt; ?>">
    <?php endif; ?>    
    <?php
    echo '<div class="caption">';
    if (get_field('cta_slide_title', 'option')) {
        echo '<h2><span>' . get_field('cta_slide_title', 'option') . '</span></h2>';
    }
    if (get_field('cta_slide_subtitle')) {
        echo '<h3><span>' . get_field('cta_slide_subtitle', 'option') . '</span></h3>';
    }
    if (get_field('cta_button_1', 'option')) {
        echo '<a id="cta_btn" href="' . get_field('cta_button_1', 'option') . '" class="btn btn-primary btn-lg" type="button">Let\'s Do This</a>';
    }
    echo '</div>';
    ?>

</div>
