<div id="myCounter" class="col-fullbleed statistical-numbers ocean">
    <div class="col-full sourcing">
        <div class="column-8 entry-content">
            <?php
            if (get_field('recruitment_statistic_title')) {
                $recruitment_statistic_title = get_field('recruitment_statistic_title');
                echo '<h2 class="text-center">' . $recruitment_statistic_title . '</h2>';
            }
            ?>
            <?php
            if (get_field('recruitment_statistic_content')) {
                $recruitment_statistic_content = get_field('recruitment_statistic_content');
                echo '<p style="text-align: center;">'.$recruitment_statistic_content.'</p>';
            }
            ?>
        </div>
        <div class="column-8">
            <div id="myCountner" class="rooster-facts">
                <?php
                if (get_field('column_1_data')) {
                    $column_1_data = get_field('column_1_data');
                } else {
                    $column_1_data = "10";
                }
                if (get_field('column_1_title')) {
                    $column_1_title = get_field('column_1_title');
                } else {
                    $candidates_sourced = "Column 1 Title";
                }
                
                if (get_field('column_2_data')) {
                    $column_2_data = get_field('column_2_data');
                } else {
                    $column_2_data = "6";
                }
                if (get_field('column_2_title')) {
                    $column_2_title = get_field('column_2_title');
                } else {
                    $column_2_title = "Column 2 Title";
                }

                if (get_field('column_3_data')) {
                    $column_3_data = get_field('column_3_data');
                } else {
                    $column_3_data = "3";
                }
                if (get_field('column_2_title')) {
                    $column_3_title = get_field('column_3_title');
                } else {
                    $column_3_title = "Column 3 Title";
                }

                if (get_field('column_4_data')) {
                    $column_4_data = get_field('column_4_data');
                } else {
                    $column_4_data = "2";
                }
                if (get_field('column_4_title')) {
                    $column_4_title = get_field('column_4_title');
                } else {
                    $column_4_title = "Column 4 Title";
                }
                ?>

                <div class="column-3 fact" data-perc="<?php echo $column_1_data; ?>">
                    <div class="factor"></div>
                    <div class="factor-title"><?php echo $column_1_title; ?></div>
                </div>
                <div class="column-3 fact" data-perc="<?php echo $column_2_data; ?>">
                    <div class="factor"></div>
                    <div class="factor-title"><?php echo $column_2_title; ?></div>
                </div>
                <div class="column-3 fact" data-perc="<?php echo $column_3_data; ?>">
                    <div class="factor"> </div>
                    <div class="factor-title"><?php echo $column_3_title; ?></div>
                </div>
                <div class="column-3 fact fact-last" data-perc="<?php echo $column_4_data; ?>">
                    <div class="factor percent"> </div>
                    <div class="factor-title"><?php echo $column_4_title; ?></div>
                </div> 

            </div>
        </div>
    </div>
</div>