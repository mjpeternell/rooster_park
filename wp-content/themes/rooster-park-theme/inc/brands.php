<section id="Cool-Stuff" class="col-fullbleed ocean companies">
    <div class="col-full">
        <?php
        if (get_field('section_title')) {
            echo '<h1 class="section-header orange">' . get_field('section_title') . '</p>';
        }
        ?>
        <?php if (have_rows('company_logo_repeater')): ?>
            <article class="column-12 text-center"> 
                <ul class="icon-list">
                    <?php
                    while (have_rows('company_logo_repeater')): the_row();
                        // vars
                        $image = get_sub_field('comapany_logo_image');
                        ?>
                        <li class="logo">
                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" class="img-responsive"/>
                        </li>
                    <?php endwhile; ?>
                </ul>
            </article>
        <?php endif; ?>
    </div>
</section>