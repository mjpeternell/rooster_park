<section class="col-fullbleed white founders-bio">
    <div class="col-full capability">
        <div class="column-8">
            <?php if (get_field('about_rp_content_area')): ?>
                <div class="entry-content"><?php the_field('about_rp_content_area'); ?></div>
            <?php endif; ?>
        </div>
    </div>
</section>