<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package RoosterPark
 * @since RoosterPark 1.0
 */
?>

    </div><!-- #main .site-main -->
</div><!-- #page .hfeed .site -->
<footer id="colophon" class="site-footer container-fluid" role="contentinfo">
    <div class="site-info">
        <?php
        $footer_args = array(
            'theme_location' => 'footer',
            'menu_class' => 'footer-menu',
            'depth' => 1,
            'walker' => new rooster_park_navwalker()
        );
        wp_nav_menu($footer_args);
        ?>
        <?php do_action('rooster_park_credits'); ?>
<!--        <div class="copyright">
            <?php //esc_attr_e('Copyright ©', 'preference'); ?><?php _e(date('Y')); ?> <?php bloginfo('name'); ?>  All Rights Reserved
        </div>-->
    </div><!-- .site-info -->
</footer><!-- #colophon .site-footer -->


<?php wp_footer(); ?>
<div class="scroll-to-top">
    <i class="fa fa-angle-up"></i>
</div>
</body>
</html>