<?php
/*
 * Hero Image: includes ACF Hero image, title, subtitle and copy.
 * Pages: Homepage,
 */
?>
<?php
if (is_front_page()) {
    $icon_pg = "front-page";
} else {
    $icon_pg = "secondary-page";
}
?>
<div class="hero static <?php echo $icon_pg; ?>">
    <?php
    $hero_desktop = get_field('hero_desktop');
    if (!empty($hero_desktop)):
        $desk_size = 'hero-image-large';
        $desk_url = $hero_desktop['url'];
        $desk_title = $hero_desktop['title'];
        $desk_alt = $hero_desktop['alt'];
        $desk_caption = $hero_desktop['caption'];
        $desk_resp_size_range_hero = "(min-width: 320px) 50vw, 100vw";
        ?>
        <img class="img-responsive desktop-hero" src="<?php echo $desk_url; ?>" alt="<?php echo $desk_alt; ?>">		
    <?php endif; ?>
    <?php
        $hero_mobile = get_field('hero_mobile');
    if (!empty($hero_mobile)):
        $mobile_size = 'hero-image-large';
        $mobile_url = $hero_mobile['url'];
        $mobile_title = $hero_mobile['title'];
        $mobile_alt = $hero_mobile['alt'];
        $mobile_caption = $hero_mobile['caption'];
        $mobile_resp_size_range_hero = "(min-width: 320px) 50vw, 100vw";
        ?>
        <img class="img-responsive mobile-hero" src="<?php echo $mobile_url; ?>" alt="<?php echo $alt; ?>">		
    <?php endif; ?>


    <?php
    echo '<div class="caption">';
    if (get_field('hero_title')) {
        echo '<h1><span>' . get_field('hero_title') . '</span></h1	>';
    }
    if (get_field('hero_subtitle')) {
        echo '<h2>' . get_field('hero_subtitle') . '</h2>';
    }
    if (get_field('hero_copy')) {
        echo '<p>' . get_field('hero_copy') . '</p>';
    }
    echo '</div>';
    ?>


</div>