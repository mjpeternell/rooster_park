<?php
/*
 * We Do Both: includes Recruit and Development Posts, title, content and link.
 * Pages: Homepage,
 * 
 */
$my_post_thumb_img = "full";
?>
<section id="We-Do-Both" class="midnight text-center two-col-section we-do col-fullbleed">
    <div class="col-full">
        <?php
                $we_d_both_section_query = new WP_Query('name=we-do-both-section');
                while ($we_d_both_section_query->have_posts()) {
                    $we_d_both_section_query->the_post();
                    the_content();
                    edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>'), '<p class="edit">', '</p>', 0, 'post-edit-link btn btn-default');
                } ?>
                <?php wp_reset_postdata(); ?>
           <div class="inner-col-full"><article class="column-2">
                <?php
                $my_dev_query = new WP_Query('name=engineering-section');
                while ($my_dev_query->have_posts()) {
                    $my_dev_query->the_post();
                    ?>
                    <header class="entry-header">
                        <?php if (has_post_thumbnail()) { ?>
                            <a href="/engineering"><?php the_post_thumbnail($my_post_thumb_img, array('class' => "svg-thumbnail ")); ?></a>
                        <?php } else { ?>
                            <a href="/engineering"><img src="https://placeholdit.imgix.net/~text?txtsize=12&txt=120%C3%97120&w=120&h=120" class="img-thumbnail"/></a>
                        <?php } ?>
                        <h1 class="entry-title"><?php the_title() ?></h1>
                    </header>
                    <div class="content"><?php the_content(); ?></div>
                    <footer><a class="btn btn-lg btn-primary" type="button" href="/engineering">Learn more</a></footer>
                    <?php edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>'), '<p class="edit">', '</p>', 0, 'post-edit-link btn btn-default'); ?>

                <?php } ?>
                <?php wp_reset_postdata(); ?>
            </article>
            <article class="column-2">
                <?php
                $my_rec_query = new WP_Query('name=recruitment');
                while ($my_rec_query->have_posts()) {
                    $my_rec_query->the_post();
                    ?>
                    <header class="entry-header">
                        <?php if (has_post_thumbnail()) { ?>
                            <a href="/recruitment"><?php the_post_thumbnail($my_post_thumb_img, array('class' => "svg-thumbnail ")); ?></a>
                        <?php } else { ?>
                            <a href="/recruitment"><img src="https://placeholdit.imgix.net/~text?txtsize=12&txt=120%C3%97120&w=120&h=120" class="img-thumbnail"/></a>
                        <?php } ?>
                        <h1 class="entry-title"><?php the_title() ?></h1>
                    </header>
                    <div class="content"><?php the_content(); ?></div>
                    <footer><a class="btn btn-lg btn-primary" type="button" href="/recruitment">Our process</a></footer>
                        <?php edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>'), '<p class="edit">', '</p>', 0, 'post-edit-link btn btn-default'); ?>
                    <?php } ?>
                    <?php wp_reset_postdata(); ?>
            </article>
        </div>
    </div>
</section>


