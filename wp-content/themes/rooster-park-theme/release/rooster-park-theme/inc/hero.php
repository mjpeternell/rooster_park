<?php

/* 
 * Hero Image: includes ACF Hero image, title, subtitle and copy.
 * Pages: Homepage,
 */

?>
<div class="jumbotron">
    <?php
    $image = get_field('hero_slide_1');

    if (!empty($image)):
        $size = 'hero-image-large';
        $url = $image['url'];
        $title = $image['title'];
        $alt = $image['alt'];
        $caption = $image['caption'];
        $resp_size_range_hero = "(min-width: 320px) 50vw, 100vw";
        ?>
        <img sizes="<?php echo $resp_size_range_hero; ?>" srcset="<?php echo $image['sizes']['hero-img-xxs']; ?> 320w, <?php echo $image['sizes']['hero-img-sm']; ?> 480w, <?php echo $image['sizes']['hero-img-medium-a']; ?> 680w, <?php echo $image['sizes']['hero-img-medium-b']; ?> 768w, <?php echo $image['sizes']['hero-img-large-a']; ?> 992w, <?php echo $image['sizes']['hero-img-large-b']; ?> 1200w, <?php echo $image['sizes']['hero-imag-xlarge']; ?> 1354w" sizes="<?php echo $resp_size_range; ?>" src="<?php echo $image['sizes']['hero-image-xlarge']; ?>" alt="<?php echo $alt; ?>">		
        <?php
        echo '<div class="caption">';
        if (get_field('hero_slide_title_1')) {
            echo '<h1><span>' . get_field('hero_slide_title_1') . '</span></h1	>';
        }
        if (get_field('hero_slide_subtitle_1')) {
            echo '<h2>' . get_field('hero_slide_subtitle_1') . '</h2>';
        }
        if (get_field('hero_slide_copy_1')) {
            echo '<p>' . get_field('hero_slide_copy_1') . '</p>';
        }
        echo '</div>';
        ?>

    <?php endif; ?>
</div>