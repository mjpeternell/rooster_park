<?php
/*
 * Hero Image: includes ACF Hero image, title, subtitle and copy.
 * Pages: Homepage,
 */
?>

<?php
$hero_alt = "";
if(!is_front_page()){
   $hero_alt = "hero-alt";
} else {
    $hero_alt = "hero-home";
}
$image = get_field('hero_slide_1');

if (!empty($image)):
    $size = 'hero-imag-xlarge';
    $parallax = $image['sizes'][$size];
    $url = $image['url'];
    $title = $image['title'];
    $alt = $image['alt'];
    $caption = $image['caption'];
    $resp_size_range_hero = "(min-width: 320px) 50vw, 100vw";
    ?>
    <div id="hero" class="<?php echo $hero_alt; ?> jumbotron parallax-window "  data-natural-width="1354" data-natural-height="575" data-image-src="<?php echo $parallax; ?>" data-speed="0.2" data-bleed="0" data-parallax="scroll">

    <?php
    echo '<div class="caption">';
    if (get_field('hero_slide_title_1')) {
        echo '<h1><span>' . get_field('hero_slide_title_1') . '</span></h1	>';
    }
    if (get_field('hero_slide_subtitle_1')) {
        echo '<h2>' . get_field('hero_slide_subtitle_1') . '</h2>';
    }
    if (get_field('hero_slide_copy_1')) {
        echo '<p>' . get_field('hero_slide_copy_1') . '</p>';
    }
    echo '</div>';
    ?>
    </div>
    <?php endif; ?>