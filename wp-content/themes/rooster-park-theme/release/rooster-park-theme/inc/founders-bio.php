<section class="col-fullbleed white founders-bio grey">
    <div class="col-full capability">
        <div class="column-8">
            <?php
            $image = get_field('founders_img');
            if (!empty($image)):
                // vars
                $url = $image['url'];
                $title = $image['title'];
                $alt = $image['alt'];

                // thumbnail
                $size = 'thumbnail';
                $thumb = $image['sizes'][$size];
                $width = $image['sizes'][$size . '-width'];
                $height = $image['sizes'][$size . '-height'];
                ?>
                <img class="founders-img" src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" />
            <?php endif; ?>
            <?php if (get_field('founders_name')): ?>
                <h1 class="founders-name"><?php the_field('founders_name'); ?></h1>
            <?php endif; ?>
            <?php if (get_field('founders_title')): ?>
                <p class="founders_title"><?php the_field('founders_title'); ?></p>
            <?php endif; ?>
            <?php if (get_field('founder_bio')): ?>
                <div class="entry-content"><?php the_field('founder_bio'); ?></div>
            <?php endif; ?>
        </div>
    </div>
</section>