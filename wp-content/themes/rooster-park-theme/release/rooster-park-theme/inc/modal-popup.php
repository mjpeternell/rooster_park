<!-- #modal is the id of a DIV defined in the code below -->

<div id="modal_screen" class="screen"></div>
<!-- #customize your modal window here -->
<div id="modal" class="modalwindow">
    <!-- Modal window Title -->
    <header>
        <h2> </h2>
        <h3> </h3>
    </header>
    <!-- close button is defined as close class -->
    <a href="#" class="close-btn"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
    <div class="content">
        <!-- Modal Window Content -->
    </div>

</div>