<section id="capabilitiesCaseStudies" class="col-fullbleed capCaseStud">
    <div class="col-full">
        <h1 class="section-header orange">Case Studies</h1>
    </div>
    <div class="col-full">
        <?php
        $ccs_arg = array(
            'post_type' => 'case_studies',
            'posts_per_page' => 6,
            'orderby' => 'post_date',
            'order' => 'date',
            'post_status' => 'publish',
        );
        $wp_ccs_query = new WP_Query($ccs_arg);
        $post_ccs_counter = -1;
        ?>
        <div class="cs-logo-list">
            <?php
            while ($wp_ccs_query->have_posts()) : $wp_ccs_query->the_post();
                $post_ccs_counter++;
                ?>
                <div class="cs-logo-list-item" data-count="<?php echo $post_ccs_counter; ?>">
                    <?php $featured_img_url = get_the_post_thumbnail_url($wp_ccs_query->ID, 'full'); ?>
                    <a class="cs-link" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" style="background-image: url('<?php echo $featured_img_url; ?>');">
                        <div class="cs-caption ocean-bg">
                            <h3><?php the_title(); ?></h3>
                            <?php
                            $acf_cs_excerpt = get_field("home_page_cs_excerpt");
                            if ($acf_cs_excerpt) {
                                echo '<p class="truncate">'.$acf_cs_excerpt.'</p>';
                            }
                            ?>
                            <span class="learn-more">Read The Case Study</span>
                        </div>
                    </a>
                </div>
            <?php endwhile; ?>
        </div>
        <?php wp_reset_postdata(); ?>
    </div>
</section>