<?php
/* 
 * Footer CTA: includes ACF Hero image, title and link.
 * Pages: Homepage,
 * 
 */
?>
<!-- Secondary Hero/Footer CTA -->
<div class="capability-cta">
    <?php
    //$cta_title = get_field('capability_cta_title');

    if (get_field('capability_cta_title')):
        echo '<div class="caption">';
        if (get_field('capability_cta_title')) {
            echo '<h2><span>' . get_field('capability_cta_title') . '</span></h2>';
        }
        if (get_field('capability_subtitle')) {
            echo '<h3><span>' . get_field('capability_subtitle') . '</span></h3>';
        }
        if (get_field('capability_button')) {
            echo '<a id="cta_btn" href="' . get_field('capability_button') . '" class="btn btn-primary btn-lg" type="button">Let\'s Do This</a>';
        }
        echo '</div>';
        ?>
    <?php endif; ?>
</div>
