
<?php
$gmap_cta_hero_image = get_field('gmap_cta_hero_image');
$gmap_cta_hero_image_mobile = get_field('gmap_cta_hero_image_mobile');

if (!empty($gmap_cta_hero_image)):
    $size = 'hero-cta-1400';
    $url = $gmap_cta_hero_image['url'];
    $title = $gmap_cta_hero_image['title'];
    $alt = $gmap_cta_hero_image['alt'];
endif; 
if (!empty($gmap_cta_hero_image_mobile)):
    $size = 'hero-cta-1400';
    $url_mob = $gmap_cta_hero_image_mobile['url'];
    $title_mob = $gmap_cta_hero_image_mobile['title'];
    $alt_mob = $gmap_cta_hero_image_mobile['alt'];
endif; 
?>
<div class="hero static">
    <?php if (!empty($gmap_cta_hero_image)): ?>
        <img class="img-responsive desktop-hero" src="<?php echo $url; ?>" alt="<?php echo $alt; ?>">	
    <?php endif; ?>
    <?php if (!empty($gmap_cta_hero_image_mobile)): ?>
        <img class="img-responsive mobile-hero" src="<?php echo $url_mob; ?>" alt="<?php echo $alt_mob; ?>">	
    <?php endif; ?>
</div>
<section class="col-fullbleed dust gmap-cta">
    <div class="col-full capability">
        <?php
        $location = get_field('gmaps_location');
        $more_contact_info = get_field('more_contact_info');

        $myAddress = $location['address'];
        ?>
        <?php if (get_field('google_map_cta_title')): ?>
            <h1><?php the_field('google_map_cta_title'); ?></h1>
        <?php endif; ?>
        <?php if (!empty($more_contact_info)): ?>
            <div class="entry-content"><?php echo $more_contact_info; ?></div>
        <?php endif; ?>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD2OzUsZyFKQk0vU3yt1mNjQQ7GQFUDe9o"></script>
        <?php
        if (($_SERVER['HTTP_HOST'] === 'rooster.mattpeternell.net') || $_SERVER['HTTP_HOST'] === 'roosterpark.com') {
            $minified = ".min";
        } else {
            $minified = "";
        }
        ?>
        <script src="<?php echo $themeUrl; ?>/wp-content/themes/rooster-park-theme/assets/js/google-maps<?php echo $minified; ?>.js" type="text/javascript"></script>
        <?php
        if (!empty($location)):
            ?>
            <div class="acf-map">
                <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
            </div>
        <?php endif; ?>
    </div>
</section>