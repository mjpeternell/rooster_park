<?php

/**
 * Template Name: Home Page - Full Width w/ Hero
 * The template used for displaying page content in page.php
 *
 * @package RoosterPark
 * @since RoosterPark 1.
 */
get_header();
?>
<?php get_template_part('inc/hero-static');?>
<?php get_template_part('inc/we-do-both'); ?>
<?php get_template_part('inc/testimonial-loop'); ?>
<?php get_template_part('inc/brands'); ?>
<?php get_template_part('inc/engineering-case-studies'); ?>
<?php //get_template_part('inc/case-study-loop-grid'); ?>
<?php get_template_part('inc/footer-cta'); ?>
<?php get_footer();
