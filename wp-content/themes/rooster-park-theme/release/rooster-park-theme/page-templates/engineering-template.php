<?php
/**
 * Template Name: Engineering Page
 *
 * The template used for displaying page content in page.php
 *
 * @package RoosterPark
 * @since RoosterPark 1.
 */
get_header();
get_template_part('inc/hero-static'); 
?>
<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <?php get_template_part('inc/engineering-dev-services'); ?>
        <?php get_template_part('inc/engineering-case-studies'); ?>
        <?php get_template_part('inc/modal-popup'); ?>
    </main><!-- .site-main -->
</div><!-- .content-area -->    
<?php get_footer(); ?>
