<?php
/**
 * Template Name: Recruitment Page
 *
 * The template used for displaying page content in page.php
 *
 * @package RoosterPark
 * @since RoosterPark 1.
 */
get_header();
get_template_part('inc/hero-static');
?>
<div id="primary" class="content-area ">
    <main id="main" class="site-main" role="main">
        <?php get_template_part('inc/recruit-recruitment'); ?>
        <?php get_template_part('inc/recruit-sourcing'); ?>
        <?php get_template_part('inc/recruit-evaluating'); ?>
        <?php get_template_part('inc/recruit-connecting'); ?>
        <?php
        while (have_posts()) : the_post();
            get_template_part('inc/recruit-my-counter');
        endwhile;
        ?>
        <?php get_template_part('inc/modal-popup'); ?>
        <?php //get_template_part('inc/footer-cta'); ?>
    </main>
</div>
<?php get_footer(); ?>
