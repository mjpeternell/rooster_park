<?php
/**
 * Template Name: Careers Page - Careers
 * The template used for displaying page content in page.php
 *
 * @package RoosterPark
 * @since RoosterPark 1.
 */
get_header();
get_template_part('inc/hero-static');
?>            
<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

        <div class="col-fullbleed grey career">
            <div class="col-full">
                <section id="careers" class="column-10">
                    <?php while (have_posts()) : the_post(); ?>
                        <?php get_template_part('content', 'page'); ?>
                        <?php //comments_template('', true); ?>
                    <?php endwhile; // end of the loop. ?>         
                </section>                                    </div>
        </div>
        <div class="col-fullbleed white">
            <div class="col-full">
                <section id="careers-posts">
                    <?php
                    $accesstoken = "c3ceb49bda5d2ddb6563029f4bd2c4f8";
                    // init curl object        
                    $ch = curl_init();
                    $headr = array();
                    $headr[] = 'Content-length: 0';
                    $headr[] = 'Content-type: application/json';
                    $headr[] = 'Authorization: Token ' . $accesstoken;

                    // define options
                    $optArray = array(
                        CURLOPT_HTTPHEADER => $headr,
                        CURLOPT_URL => "https://api.catsone.com/v3/portals/8345/jobs?per_page=100",
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_TIMEOUT => 1000,
                    );

                    // apply those options
                    curl_setopt_array($ch, $optArray);

                    // execute request and get response
                    $result = curl_exec($ch);
                    //var_dump(json_decode($result));                       
                    $result_array = (json_decode($result, true));
                    
                    ?>
                    <div class="job-list-container">
                        <div class="job-list">
                            <div class="job-header">
                                <div class="th-1"><span>Job Title</span></div>
                                <div class="th-2"><span>Location</span></div>
                            </div>
                            <div class="job-postings">
                                <?php
                                $count = -1;

                                function truncate($text, $chars = 300) {
                                    $text = $text . " ";
                                    $text = substr($text, 0, $chars);
                                    $text = substr($text, 0, strrpos($text, ' '));
                                    $text = $text . "...";
                                    return $text;
                                }

                                foreach ($result_array['_embedded']['jobs'] as $value) {

                                    //[company_id] => 2608289
                                    $even = "even";
                                    $odd = "odd";
                                    $ulink = "";
                                    $description = strip_tags($value['description']);
                                    $active_job = $value['_embedded']['status']['mapping'];
                                    $job_title = $value['title'];
                                    $local_st = $value['location']['state'];
                                    $local_city = $value['location']['city'];

                                    if ($value['id']) {
                                        $ulink = $value['id'];
                                    }

                                    if ($active_job == "active") {
//                                        echo '<pre style="font-size: 10px;">';
//                                        print_r($value);
//                                        echo "</pre>";
                                        $count++;
                                        echo '<div class="job-post ' . (($count % 2) == 0 ? $odd : $even) . ' " data-count="' . $count . '" data-job-status="' . $active_job . '" data-job-id="' . $ulink . '">';
                                        echo '<div class="link">';
                                        echo '<a href="http://roosterpark.catsone.com/careers/index.php?m=portal&a=details&jobOrderID=' . $ulink . '" title="' . $job_title . '" target="_blank">' . $job_title . '</a>';
                                        echo '</div>';
                                        echo '<div class="local">' . $local_city . ', ' . $local_st . '</div>';
                                        echo '<div class="description">';
                                        echo '<p>' .truncate($description,300). '</p>';
                                        echo '<p>' . $short_desc ?: $short_desc . '</p>';
                                        echo '</div>';
                                        echo '</div>';
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>

                    <?php
                    
//                    echo '<pre>';
//                    print_r($result_array['_embedded']);
//                    echo '</pre>';
                    // also get the error and response code
                    $errors = curl_error($ch);
                    $response = curl_getinfo($ch, CURLINFO_HTTP_CODE);

                    curl_close($ch);

//                    var_dump($errors);
//                    var_dump($response);
                    ?>
                </section>
            </div>
        </div>

    </main><!-- .site-main -->
</div><!-- .content-area -->  
<?php
get_template_part('inc/footer-cta');
get_footer();


