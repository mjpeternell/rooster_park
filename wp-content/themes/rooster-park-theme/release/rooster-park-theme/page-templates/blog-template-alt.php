<?php
/**
 * Template Name: Blog Page - Rooster Blog Alt
 * The template used for displaying page content in page.php
 *
 * @package RoosterPark
 * @since RoosterPark 1.
 */
get_header();
get_template_part('inc/hero-static');
?>
<div id="primary" class="content-area">
    <main id="main" class="site-main white" role="main">
        <section id="Rooster-Blog" class="col-full rooster-blog">
            <div class="column-10 offset-1">
                <!--            <div class="col-full">-->


                <?php
                // Start the loop.
                while (have_posts()) : the_post();
                // Include the page content template.
                //get_template_part('content', 'page');
                // End the loop.
                endwhile;
                ?>

                <div class="row">

                    <?php
                    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
                    $blog_arg = array(
                        'post_type' => 'rooster_blog',
                        'orderby' => 'post_date',
                        'order' => 'date',
                        'post_status' => 'publish',
                        'posts_per_page' => 12,
                        'paged' => $paged,
                    );
                    $wp_blog_query = new WP_Query($blog_arg);
                    $postx_counter = 0;
                    if (have_posts()) :
                        while ($wp_blog_query->have_posts()) : $wp_blog_query->the_post();
                            $postx_counter++;
                            $offset = "";
                            ?>
                            <div class=" column-tile bg" data-count="<?php echo $postx_counter; ?>">
                                <article class="tile-inner" >

                                    <div class="column-12 content">

                                        <h1 class="title"><a class="" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                                        <div class="blog-content">
                                            <?php the_excerpt(); ?>
                                        </div>
                                        <footer class="entry-meta">
                                            <a class="btn btn-link blog-link" href="<?php the_permalink(); ?>" role="button">Read More</a>
                                            <?php edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>'), '', '', 0, 'post-edit-link btn btn-default'); ?>
                                        </footer>
                                    </div>

                                </article>
                            </div>

                            <?php
                            if ($postx_counter) {
                                echo "<hr>";
                            }
                            ?>
                        <?php endwhile;
                        ?>
                    </div>  

                <?php endif; ?>
                <?php if ($wp_blog_query->max_num_pages > 1) : // check if the max number of pages is greater than 1   ?>
                    <div class="row">

                        <nav class="post-navigation column-6 offset-2">
                            <div class="nav-previous">
                                <?php echo get_next_posts_link('<i class="fa fa-angle-left" aria-hidden="true"></i> Older Entries', $wp_blog_query->max_num_pages); // display older posts link    ?>
                            </div>
                            <div class="nav-next">
                                <?php echo get_previous_posts_link('Newer Entries <i class="fa fa-angle-right" aria-hidden="true"></i>'); // display newer posts link    ?>
                            </div>
                        </nav>

                    </div>
                <?php endif; ?>
                <?php wp_reset_postdata(); ?>
        </section>
    </main><!-- .site-main -->
</div>
<?php
//get_template_part('inc/footer-cta');
get_footer();
?>
