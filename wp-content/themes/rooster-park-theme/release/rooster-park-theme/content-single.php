<?php
/**
 * @package RoosterPark
 * @since RoosterPark 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header">
        <h1 class="entry-title"><?php the_title(); ?></h1>
        <?php if (is_singular('staff')) : ?>
            <div class = "popup-job-title">
                <?php $job_title = get_field('job_title'); ?>
                <?php if (!empty($job_title)): ?>
                    <?php echo $job_title; ?>
                <?php endif; ?>
            </div>  
        <?php endif ?>
    </header><!-- .entry-header -->

    <div class="entry-content">
        <?php the_content(); ?>
        <?php wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'rooster_park'), 'after' => '</div>')); ?>
    </div><!-- .entry-content -->

    <footer class="entry-meta">
        <?php
        /* translators: used between list items, there is a space after the comma */
        $category_list = get_the_category_list(__(', ', 'rooster_park'));

        /* translators: used between list items, there is a space after the comma */
        $tag_list = get_the_tag_list('', __(', ', 'rooster_park'));
        if (!is_singular('staff')) {


            if (!rooster_park_categorized_blog()) {
                // This blog only has 1 category so we just need to worry about tags in the meta text
                if ('' != $tag_list) {
                    $meta_text = __('This entry was tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'rooster_park');
                } else {
                    $meta_text = __('Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'rooster_park');
                }
            } else {
                // But this blog has loads of categories so we should probably display them here
                if ('' != $tag_list) {
                    $meta_text = __('This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'rooster_park');
                } else {
                    $meta_text = __('This entry was posted in %1$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'rooster_park');
                }
            }
        }
// end check for categories on this blog

//        printf(
//                $meta_text, $category_list, $tag_list, get_permalink(), the_title_attribute('echo=0')
//        );
        ?>

        <?php edit_post_link(__('Edit', 'rooster_park'), '<span class="edit-link">', '</span>'); ?>
    </footer><!-- .entry-meta -->
</article><!-- #post-<?php the_ID(); ?> -->
