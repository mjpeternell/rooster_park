<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package RoosterPark
 * @since RoosterPark 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php if (!is_page('careers')): ?>
        <header class = "entry-header">
            <h1 class = "entry-title"><?php the_title(); ?></h1>
        </header><!-- .entry-header -->      
    <?php endif; ?>
    <div class="entry-content">
        <?php the_content(); ?>
        <?php wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'rooster_park'), 'after' => '</div>')); ?>
        <?php edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>'), '<p class="edit">', '</p>', 0, 'post-edit-link btn btn-default'); ?>
    </div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->
