<?php
/**
 * The Template for displaying all single posts.
 *
 * @package RoosterPark
 * @since RoosterPark 1.0
 */
get_header();
?>
<div id="primary" class="content-area">
    <main id="content" class="site-content" role="main">
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <div class="col-fullbleed">
                <div class="rooster-archive">

                    <script>
                        jQuery(document).ready(function () {
                            if (jQuery("#js-parallax-window").length) {
                                parallax();
                            }
                        });

                        jQuery(window).scroll(function (e) {
                            if (jQuery("#js-parallax-window").length) {
                                parallax();
                            }
                        });

                        function parallax() {
                            if (jQuery("#js-parallax-window").length > 0) {
                                var plxBackground = jQuery("#js-parallax-background");
                                var plxWindow = jQuery("#js-parallax-window");

                                var plxWindowTopToPageTop = jQuery(plxWindow).offset().top;
                                var windowTopToPageTop = jQuery(window).scrollTop();
                                var plxWindowTopToWindowTop = plxWindowTopToPageTop - windowTopToPageTop;

                                var plxBackgroundTopToPageTop = jQuery(plxBackground).offset().top;
                                var windowInnerHeight = window.innerHeight;
                                var plxBackgroundTopToWindowTop = plxBackgroundTopToPageTop - windowTopToPageTop;
                                var plxBackgroundTopToWindowBottom = windowInnerHeight - plxBackgroundTopToWindowTop;
                                var plxSpeed = 0.35;

                                plxBackground.css('top', -(plxWindowTopToWindowTop * plxSpeed) + 'px');
                            }
                        }

                    </script>
                    <?php
                    if (has_post_thumbnail()) :
                        /* grab the url for the full size featured image */
                        $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'full');
                        ?>
                        <div id="js-parallax-window" class="parallax-window">
                            <div class="parallax-static-content">
                                <h1 class="entry-title"><?php the_title(); ?></h1>
                            </div>
                            <div id="js-parallax-background" class="parallax-background " style="background-image: url('<?php echo $featured_img_url; ?>');"></div>
                        </div>
                    <?php else: ?>
                        <div class="single-hero">
                            <header id="singleHero" class="entry-header" >
                                <h1 class="entry-title"><?php the_title(); ?></h1>
                            </header><!-- .entry-header -->
                        </div>
                    <?php endif; ?>

                </div>
            </div>
            <div class="col-fullbleed">
                <div class="col-full rooster-archive">
                    <div class="column-8 offset-2">
                        <?php while (have_posts()) : the_post(); ?>
                            <?php //rooster_park_content_nav('nav-above');   ?>
                            <div class="entry-content">
                                <div class="cs-block cs-brief">
                                    <?php the_content(); ?>
                                </div>
                                <?php wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'rooster_park'), 'after' => '</div>')); ?>
                            </div><!-- .entry-content -->
                            <footer class="entry-meta">
                                <?php
                                /* translators: used between list items, there is a space after the comma */
                                $category_list = get_the_category_list(__(', ', 'rooster_park'));

                                /* translators: used between list items, there is a space after the comma */
                                $tag_list = get_the_tag_list('', __(', ', 'rooster_park'));

//                                if (!rooster_park_categorized_blog()) {
//                                    // This blog only has 1 category so we just need to worry about tags in the meta text
//                                    if ('' != $tag_list) {
//                                        $meta_text = __('This entry was tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'rooster_park');
//                                    } else {
//                                        $meta_text = __('Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'rooster_park');
//                                    }
//                                } else {
//                                    // But this blog has loads of categories so we should probably display them here
//                                    if ('' != $tag_list) {
//                                        $meta_text = __('This entry was posted in %1$s and tagged %2$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'rooster_park');
//                                    } else {
//                                        $meta_text = __('This entry was posted in %1$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'rooster_park');
//                                    }
//                                } // end check for categories on this blog
//                                printf(
//                                        $meta_text, $category_list, $tag_list, get_permalink(), the_title_attribute('echo=0')
//                                );
                                ?>
                                <?php edit_post_link(__('Edit <i class="fa fa-pencil-square-o"></i>'), '<p class="edit">', '</p>', 0, 'post-edit-link btn btn-default'); ?>
                            </footer><!-- .entry-meta -->
                            <?php //rooster_park_content_nav('nav-below');  ?>
                        <?php endwhile; // end of the loop.   ?>
                    </div>
                </div>
            </div>
            <div class="col-full single-blog-nav below bot-margin">
                <div class="column-8 offset-2 text-center">
                    <?php rooster_park_content_nav('nav-below'); ?>
                </div>
            </div>

        </article><!-- #post-<?php the_ID(); ?> -->
    </main><!-- #content .site-content -->
</div><!-- #primary .content-area -->
<?php
get_template_part('inc/footer-cta');
get_footer();
?>