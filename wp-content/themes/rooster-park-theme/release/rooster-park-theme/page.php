<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package RoosterPark
 * @since RoosterPark 1.0
 */
get_header();
?>
<div id="primary" class="content-area">
    <main id="main" class="site-main white" role="main">
        <div class="col-fullbleed rooster-blog">
            <div class="col-full">
                <div class="column-8">
                    <?php while (have_posts()) : the_post(); ?>
                        <?php get_template_part('content', 'page'); ?>
                        <?php //comments_template('', true); ?>
                    <?php endwhile; // end of the loop. ?>
                </div>
                <div class="column-4 sidebar">
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>
    </main>
</div>
<?php get_footer(); ?>