<?php
/**
 * The Template for displaying all single posts.
 *
 * @package RoosterPark
 * @since RoosterPark 1.0
 */
get_header();
?>
<div id="primary" class="content-area">
    <main id="content" class="site-content" role="main">
        <?php if (has_post_thumbnail()) { ?>
            <div class="hero static">
                <?php the_post_thumbnail('instagram-square', array('class' => 'img-responsive')); ?></div>
        <?php } ?>
        <div class="col-fullbleed white">
            <?php while (have_posts()) : the_post(); ?>

                <div class="col-full single-blog-post">
                    <div class="column-8 offset-2">
                        <div class="row">
                            <div class="column-12">
                                <?php get_template_part('content', 'single'); ?>
                            </div>
                        </div>
                    </div> 
                </div>

                <div class="col-full single-blog-nav below">
                    <div class="column-8 offset-2 text-center">
                        <?php rooster_park_content_nav('nav-below'); ?>
                    </div>
                </div>
                <div class="col-full single-blog-nav top">
                    <div class="column-8 offset-2 text-left single-nav-link staff">
                        <a href="/about/#our-team" type="button" class="btn btn-link single-nav-link"><i class="fa fa-angle-left" aria-hidden="true"></i> Back To Meet Our Team</a>
                    </div>
                </div>
            <?php endwhile; // end of the loop.  ?>
        </div>
    </main><!-- #content .site-content -->
</div><!-- #primary .content-area -->
<?php
//get_template_part('inc/footer-cta-single');
get_footer();
?>